import { Home } from './Models/home';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public home:Home;
  constructor() {
    this.home = {
      homeImage:{
          img:"https://www.pngkey.com/png/full/803-8031803_black-sabbath-logo-live.png",
          name:"Black Sabbath Fallen Angel"
      }
    }
  }

  ngOnInit(): void {
  }

}
