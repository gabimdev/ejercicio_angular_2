export interface Home {
  homeImage: ImageHome,
}

export interface ImageHome{
  img: string,
  name: string,
}
