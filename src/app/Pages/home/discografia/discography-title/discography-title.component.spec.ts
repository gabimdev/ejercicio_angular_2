import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DiscographyTitleComponent } from './discography-title.component';

describe('DiscographyTitleComponent', () => {
  let component: DiscographyTitleComponent;
  let fixture: ComponentFixture<DiscographyTitleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DiscographyTitleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DiscographyTitleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
