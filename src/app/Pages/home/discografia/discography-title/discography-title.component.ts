import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-discography-title',
  templateUrl: './discography-title.component.html',
  styleUrls: ['./discography-title.component.scss']
})
export class DiscographyTitleComponent implements OnInit {

  @Input() title!:string;
  constructor() { }

  ngOnInit(): void {
  }

}
