
export interface Discografia {
  discographyEvent: Event
  title: string,
  discList: Array<Disc>
}


export interface Event{
  btnName:string;
  eventText:string;
}

export interface Disc {
  cover: Image,
  title:string,
  originalFormation: boolean,
  live:boolean
}

export interface Image {
  img: string,
  name: string
}
