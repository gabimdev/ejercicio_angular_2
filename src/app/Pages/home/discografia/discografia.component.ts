
import { Discografia } from './Models/discografia';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-discografia',
  templateUrl: './discografia.component.html',
  styleUrls: ['./discografia.component.scss']
})
export class DiscografiaComponent implements OnInit {


  public btnFormation = true;
  public discografia: Discografia;
  public sonMessage:any;
  public original:any;
  public click: boolean =true;

  constructor() {
    this.discografia = {
      discographyEvent:{
        btnName: "Discos con la Formación Original",
        eventText: "El titulo del disco es"
      },
      title: "Discografia",
      discList: [
        {
          cover: {
            img:"https://blacksabbathcom.s3.amazonaws.com/content/discography/black_sabbath_0.jpg",
            name:"Black Sabbath"
          },
          title:"Black Sabbath",
          originalFormation: true,
          live: false
        },
        {
          cover: {
            img:"https://blacksabbathcom.s3.amazonaws.com/content/discography/paranoid.jpg",
            name:"Paranoid"
          },
          title:"Paranoid",
          originalFormation: true,
          live: false
        },
        {
          cover: {
            img:"https://blacksabbathcom.s3.amazonaws.com/content/discography/master_of_reality.jpg",
            name:"Master of Reality"
          },
          title:"Master of Reality",
          originalFormation: true,
          live: false
        },
        {
          cover: {
            img:"https://blacksabbathcom.s3.amazonaws.com/content/discography/black_sabbath_vol_4.jpg",
            name:"Vol 4"
          },
          title:"Vol 4",
          originalFormation: true,
          live: false
        },
        {
          cover: {
            img:"https://blacksabbathcom.s3.amazonaws.com/content/discography/sabbath_bloody_sabbath.jpg",
            name:"Sabbath Blody Sabbath"
          },
          title:"Sabbath Blody Sabbath",
          originalFormation: true,
          live: false
        },
        {
          cover: {
            img:"https://blacksabbathcom.s3.amazonaws.com/content/discography/sabotage.jpg",
            name:"Sabotage"
          },
          title:"Sabotage",
          originalFormation: true,
          live: false
        },
        {
          cover: {
            img:"https://blacksabbathcom.s3.amazonaws.com/content/discography/technical_ecstasy.jpg",
            name:"Technical Ecstasy"
          },
          title:"Technical Ecstasy",
          originalFormation: true,
          live: false
        },
        {
          cover: {
            img:"https://blacksabbathcom.s3.amazonaws.com/content/discography/never_say_die.jpg",
            name:"Never Say Die"
          },
          title:"Never Say Die",
          originalFormation: true,
          live: false
        },
        {
          cover: {
            img:"https://m.media-amazon.com/images/I/41e61aR4oIL._AC_UY436_FMwebp_QL65_.jpg",
            name:"Heaven & Hell"
          },
          title:"Heaven & Hell",
          originalFormation: false,
          live: false
        },
        {
          cover: {
            img:"https://m.media-amazon.com/images/I/51qEy00cmSL._AC_UY436_FMwebp_QL65_.jpg",
            name:"Mob Rules"
          },
          title:"Mob Rules",
          originalFormation: false,
          live: false
        },
        {
          cover: {
            img:"https://m.media-amazon.com/images/I/41GQfOXRvKL._AC_UY436_FMwebp_QL65_.jpg",
            name:"Born Again"
          },
          title:"Borna Again",
          originalFormation: false,
          live: false
        },
        {
          cover: {
            img:"https://m.media-amazon.com/images/I/71-6H0XkVYL._AC_UY436_FMwebp_QL65_.jpg",
            name:"Seventh Star"
          },
          title:"Seventh Star",
          originalFormation: false,
          live: false
        },
        {
          cover: {
            img:"https://m.media-amazon.com/images/I/41rM2a6SqQL._AC_UY436_FMwebp_QL65_.jpg",
            name:"The Eternal Idol"
          },
          title:"The Eternal Idol",
          originalFormation: false,
          live: false
        },
        {
          cover: {
            img:"https://m.media-amazon.com/images/I/512C2cstmWL._AC_UY436_FMwebp_QL65_.jpg",
            name:"Headless Cross"
          },
          title:"Headless Cross",
          originalFormation: false,
          live: false
        },
        {
          cover: {
            img:"https://m.media-amazon.com/images/I/51om1+DyPqL._AC_UY436_FMwebp_QL65_.jpg",
            name:"Tyr"
          },
          title:"Tyr",
          originalFormation: false,
          live: false
        },
        {
          cover: {
            img:"https://m.media-amazon.com/images/I/51Pxv+uf4uL._AC_UY436_FMwebp_QL65_.jpg",
            name:"Dehumanizer"
          },
          title:"Dehumanizer",
          originalFormation: false,
          live: false
        },
        {
          cover: {
            img:"https://img.discogs.com/r2g-o3qjAYTX_tNYiBAF5viNxLc=/fit-in/600x593/filters:strip_icc():format(jpeg):mode_rgb():quality(90)/discogs-images/R-2710580-1491910544-2229.jpeg.jpg",
            name:"Forbidden"
          },
          title:"Forbidden",
          originalFormation: false,
          live: false
        },
        {
          cover: {
            img:"https://blacksabbathcom.s3.amazonaws.com/content/discography/13.jpg",
            name:"13"
          },
          title:"13",
          originalFormation: true,
          live: false
        },
        {
          cover: {
            img:"https://m.media-amazon.com/images/I/41zAyDQ81uL._AC_UY436_FMwebp_QL65_.jpg",
            name:"Cross Purposes"
          },
          title:"Cross Purposes",
          originalFormation: false,
          live: false
        },
        {
          cover: {
            img:"https://images-na.ssl-images-amazon.com/images/I/515VHJn7F9L._SX425_.jpg",
            name:"Live Evil"
          },
          title:"Live Evil",
          originalFormation: false,
          live: true
        },
        {
          cover: {
            img:"https://blacksabbathcom.s3.amazonaws.com/content/discography/reunion.jpg",
            name:"Reunion"
          },
          title:"Reunion",
          originalFormation: true,
          live: true
        },



      ]
    }
  }

  ngOnInit(): void {
  }
  setMessage(message:string){
    this.sonMessage = message;
  }

  originalFormation(){
    if(this.click){
    this.original = "original";
  }else{
    this.original = "";
  }
  this.click =!this.click
}


}
