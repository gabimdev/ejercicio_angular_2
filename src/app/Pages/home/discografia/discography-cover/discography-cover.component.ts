import { Image } from './../Models/discografia';

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-discography-cover',
  templateUrl: './discography-cover.component.html',
  styleUrls: ['./discography-cover.component.scss']
})
export class DiscographyCoverComponent implements OnInit {

  @Input() cover!:Image;
  @Input() formation!:boolean;
  @Input() newClass!:string;


  @Output() emitMessage = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
  }

sendMessage(message: string){
  this.emitMessage.emit(message);
}
}
