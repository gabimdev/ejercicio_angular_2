import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DiscographyCoverComponent } from './discography-cover.component';

describe('DiscographyCoverComponent', () => {
  let component: DiscographyCoverComponent;
  let fixture: ComponentFixture<DiscographyCoverComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DiscographyCoverComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DiscographyCoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
