import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoriaGaleriaComponent } from './historia-galeria.component';

describe('HistoriaGaleriaComponent', () => {
  let component: HistoriaGaleriaComponent;
  let fixture: ComponentFixture<HistoriaGaleriaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HistoriaGaleriaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoriaGaleriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
