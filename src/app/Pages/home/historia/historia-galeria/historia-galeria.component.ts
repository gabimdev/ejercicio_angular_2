import { Image } from './../models/historia';

import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-historia-galeria',
  templateUrl: './historia-galeria.component.html',
  styleUrls: ['./historia-galeria.component.scss']
})
export class HistoriaGaleriaComponent implements OnInit {

  @Input () img!: Image;
  constructor() { }

  ngOnInit(): void {
  }

}
