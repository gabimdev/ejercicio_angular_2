
export interface Historia {
  title: string;
  imageHistory: Array<Image>;
  textHistory: string;
}

export interface Image {
  img: string;
  name: string;
}
