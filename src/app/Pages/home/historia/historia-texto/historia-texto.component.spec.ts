import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoriaTextoComponent } from './historia-texto.component';

describe('HistoriaTextoComponent', () => {
  let component: HistoriaTextoComponent;
  let fixture: ComponentFixture<HistoriaTextoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HistoriaTextoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoriaTextoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
