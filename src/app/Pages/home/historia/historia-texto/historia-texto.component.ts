import { Historia } from './../models/historia';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-historia-texto',
  templateUrl: './historia-texto.component.html',
  styleUrls: ['./historia-texto.component.scss']
})
export class HistoriaTextoComponent implements OnInit {

  @Input() history!: string;
  constructor() { }

  ngOnInit(): void {
  }

}
