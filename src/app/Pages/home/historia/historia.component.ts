import { Historia } from './models/historia';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-historia',
  templateUrl: './historia.component.html',
  styleUrls: ['./historia.component.scss']
})
export class HistoriaComponent implements OnInit {

  public historia:Historia
  constructor() {
    this.historia = {
      title: "Historia",
      imageHistory:
        [
          {
            img: "https://upload.wikimedia.org/wikipedia/commons/thumb/f/fc/Black_Sabbath_%281970%29.jpg/600px-Black_Sabbath_%281970%29.jpg",
            name:"balck sabbath 1"
          },
          {
            img: "https://www.blacksabbath.com/templates/default/images/history_7.jpg",
            name:"balck sabbath 2"
          }
        ],
      textHistory: "Black Sabbath fue una banda británica de heavy metal formada en 1968 en Birmingham por Tony Iommi (guitarra), Ozzy Osbourne (voz), Geezer Butler (bajo) y Bill Ward (batería)."+
      " Influenciados por la música de Blue Cheer, Cream o Vanilla Fudge, la agrupación incorporó desde sus inicios letras sobre ocultismo y terror con guitarras afinadas de modo más grave y consiguió varios discos de oro y platino en la década de 1970. Son los pioneros del heavy metal junto a otros grupos contemporáneos como Deep Purple,​ Budgie, Pentagram,​ Sir Lord Baltimore y Led Zeppelin. Al ser una de las primeras y más influyentes bandas de heavy metal de todos los tiempos, Black Sabbath ayudó a desarrollar el género con publicaciones tales como Paranoid, álbum que logró cuatro certificaciones de platino de la RIAA.​ Han vendido más de 75 millones de discos en todo el mundo, incluidos 15 millones de copias solo en los Estados Unidos.​"
    }
  }

  ngOnInit(): void {
  }

}
