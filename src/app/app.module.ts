import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './Components/header/header.component';
import { FooterComponent } from './Components/footer/footer.component';
import { HomeComponent } from './Pages/home/home.component';
import { HistoriaComponent } from './Pages/home/historia/historia.component';
import { DiscografiaComponent } from './Pages/home/discografia/discografia.component';
import { HistoriaTextoComponent } from './Pages/home/historia/historia-texto/historia-texto.component';
import { HistoriaGaleriaComponent } from './Pages/home/historia/historia-galeria/historia-galeria.component';
import { DiscographyTitleComponent } from './Pages/home/discografia/discography-title/discography-title.component';
import { DiscographyCoverComponent } from './Pages/home/discografia/discography-cover/discography-cover.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    HistoriaComponent,
    DiscografiaComponent,
    HistoriaTextoComponent,
    HistoriaGaleriaComponent,
    DiscographyTitleComponent,
    DiscographyCoverComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
