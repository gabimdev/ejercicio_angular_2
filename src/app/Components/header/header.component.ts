import { Component, OnInit } from '@angular/core';
import { Header } from './Models/header';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public header: Header;
  constructor() {
      this.header = {
        icon: {
          img: "https://upload.wikimedia.org/wikipedia/commons/thumb/5/5b/Black_Sabbath_logo.svg/1200px-Black_Sabbath_logo.svg.png",
          name: "black sababath logo",
        },
        links: [
          {
            linkName: "Historia",
            linkUrl:""
          },
          {
            linkName: "Discografia",
            linkUrl:""
          }
        ]
      }
    }




  ngOnInit(): void {
  }


}
