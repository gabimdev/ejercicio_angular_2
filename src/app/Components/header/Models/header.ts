export interface Header {
  icon: Icon,
  links: Array <Links>
}

export interface Icon {
  img: string,
  name: string
}

export interface Links{
  linkName: string,
  linkUrl: string
}

