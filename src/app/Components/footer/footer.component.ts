import { Footer } from './Models/footer';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  public footer:Footer;
  constructor() {
    this.footer={
    links:[
      {
        linkName:"Aviso Legal",
        linkUrl:""
      },
      {
        linkName:"Cookies",
        linkUrl:""
      },
      {
        linkName:"Contacto",
        linkUrl:""
      }
  ]
}
  }


  ngOnInit(): void {
  }

}
