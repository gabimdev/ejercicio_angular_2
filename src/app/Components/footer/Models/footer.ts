export interface Footer {
  links: Array<Links>
}

export interface Links{
  linkName:string;
  linkUrl:string
}

